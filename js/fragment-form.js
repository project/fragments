/**
 * @file
 * Behaviors for setting summaries on fragment form.
 */

(($, Drupal) => {
  /**
   * @type {Drupal~behavior}
   *
   * @prop {Drupal~behaviorAttach} attach
   *   Attaches summary behaviors on content type edit forms.
   */
  Drupal.behaviors.fragmentForm = {
    attach(context) {
      const $context = $(context);

      $context
        .find('.fragment-form-publishing-status')
        .drupalSetSummary((context) => {
          const $statusContext = $(context);
          const statusCheckbox = $statusContext.find('#edit-status-value');

          if (statusCheckbox.is(':checked')) {
            return Drupal.t('Published');
          }

          return Drupal.t('Not published');
        });

      $context
        .find('.fragment-form-authoring-information')
        .drupalSetSummary((context) => {
          const $authorContext = $(context);
          const authorField = $authorContext.find('input');

          if (authorField.val().length) {
            return authorField.val();
          }
        });
    },
  };
})(jQuery, Drupal);
